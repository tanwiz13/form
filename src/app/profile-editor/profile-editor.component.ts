import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl } from '@angular/forms';
import { Validators } from '@angular/forms';

@Component({
  selector: 'app-profile-editor',
  templateUrl: './profile-editor.component.html',
  styleUrls: ['./profile-editor.component.css']
})
export class ProfileEditorComponent implements OnInit {
  profileForm : FormGroup;
  constructor() { 
    this.profileForm = new FormGroup({

      firstname:new FormControl('',[
        Validators.required,
        Validators.minLength(4), ]),
      lastname:new FormControl('',[
        Validators.required,
        Validators.minLength(4),  
      ]),
      email:new FormControl('',[Validators.required,Validators.email]),
      empid:new FormControl('',[Validators.required]),
      password:new FormControl('',[
        Validators.required,
        Validators.minLength(4),  
      ]),
      cpassword:new FormControl('',[
        Validators.required,
        Validators.minLength(4),  
      ]),
      gender:new FormControl('',[
        Validators.required,
        Validators.minLength(4),  
      ]),
    })
  }

  ngOnInit() {
  }

}
